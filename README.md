# mysql-basic-practice

本教程以学生管理平台为依据来练习MySQL的相关知识，在开始学习之前，请先学习以下知识：

## MySQL基础知识：
##### 1. [数据库概述](https://github.com/skill-courses/DbTrainingCamp_MySQL/blob/master/DbBasics/1.%E6%95%B0%E6%8D%AE%E5%BA%93%E6%A6%82%E8%BF%B0.md)
##### 2. [MySQL概述](https://github.com/skill-courses/DbTrainingCamp_MySQL/blob/master/DbBasics/2.MySQL%E6%A6%82%E8%BF%B0.md)
##### 3. [Schema与Table](https://github.com/skill-courses/DbTrainingCamp_MySQL/blob/master/DbBasics/3.Schema%E4%B8%8ETable.md)
##### 4. [数据类型](https://github.com/skill-courses/DbTrainingCamp_MySQL/blob/master/DbBasics/4.%E6%95%B0%E6%8D%AE%E7%B1%BB%E5%9E%8B.md)
##### 5. [检索数据](https://github.com/skill-courses/DbTrainingCamp_MySQL/blob/master/DbBasics/5.%E6%A3%80%E7%B4%A2%E6%95%B0%E6%8D%AE.md)
##### 6. [数据增删改操作](https://github.com/skill-courses/DbTrainingCamp_MySQL/blob/master/DbBasics/6.%E6%95%B0%E6%8D%AE%E5%A2%9E%E5%88%A0%E6%94%B9%E6%93%8D%E4%BD%9C.md)
##### 7. [数据处理](https://github.com/skill-courses/DbTrainingCamp_MySQL/blob/master/DbAdvancedOperations/1.%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86.md)
##### 8. [高级查询](https://github.com/skill-courses/DbTrainingCamp_MySQL/blob/master/DbAdvancedOperations/2.%E9%AB%98%E7%BA%A7%E6%9F%A5%E8%AF%A2.md)
##### 9. [索引与约束](https://github.com/skill-courses/DbTrainingCamp_MySQL/blob/master/DbAdvancedOperations/3.%E7%B4%A2%E5%BC%95%E4%B8%8E%E7%BA%A6%E6%9D%9F.md)
##### 10. [视图](https://github.com/skill-courses/DbTrainingCamp_MySQL/blob/master/DbAdvancedOperations/4.%E8%A7%86%E5%9B%BE.md)
##### 11. [事务](https://github.com/skill-courses/DbTrainingCamp_MySQL/blob/master/DbAdvancedOperations/5.%E4%BA%8B%E5%8A%A1.md)
##### 12. [SQL 规范](https://github.com/skill-courses/coding-rules/blob/master/SQL/sql_sule.md)
##### 13. [Java链接数据库](https://github.com/skill-courses/DbTrainingCamp_MySQL/blob/master/DbDesign/4.%E6%95%B0%E6%8D%AE%E5%BA%93%E8%BF%9E%E6%8E%A5.md)

## 仓库说明：
##### tasks目录：该目录里面包含了所有需要同学们完成的任务，同学们可以按照顺序完成。

